#ifndef CONNECTION_H
#define CONNECTION_H

#include <string>
#include <iostream>

/*!
************************************************
 \class Connection.
 \brief Class for possibile comunication
 connections.
************************************************
*/

class Connection
{
public:
    Connection() {}
    virtual ~Connection() {}

    virtual bool	isOpen  (				) = 0;
    virtual void	write   ( const std::string &command	) = 0;
    virtual std::string	read	( const std::string &command	) = 0;
};

#endif /* CONNECTION_H */
