#include "PowerSupply.h"
#include "Channel.h"

/*!
************************************************
* PowerSupply constructor.
************************************************
*/
PowerSupply::PowerSupply(std::string model, const pugi::xml_node configuration)
    : fModel(model), fConfiguration(configuration)
{
    ;
}

/*!
************************************************
* PowerSupply distructor.
************************************************
*/
PowerSupply::~PowerSupply(void)
{
    for(auto it: fChannelMap)
        delete it.second;
    fChannelMap.clear();
}

/*!
************************************************
 * Get pointer to channel by id
 \param id Unique channel id
 \return Pointer to channel
************************************************
*/
Channel* PowerSupply::getChannel(const std::string& id)
{
    if(fChannelMap.find(id) == fChannelMap.end())
    {
        throw std::out_of_range("No channel with id " + id + " has been configured!");
    }
    return fChannelMap.find(id)->second;
}

/*!
************************************************
************************************************
*/

/*!
************************************************
* Some power supplies might not need to be turned on, so providing a default that does nothing
************************************************
*/
bool PowerSupply::turnOn(void) { return true; }

/*!
************************************************
* Some power supplies might not need to be turned off, so providing a default that does nothing
************************************************
*/
bool PowerSupply::turnOff(void) { return true; }

/*!
************************************************
************************************************
*/

std::string PowerSupply::getModel() const { return fModel; }

/*!
************************************************
************************************************
*/
std::string PowerSupply::getID(void) const { return fId; }

/*!
************************************************
************************************************
*/
void PowerSupply::setPowerType(std::string val) { fPowerType = val; }

/*!
************************************************
************************************************
*/
std::string PowerSupply::getPowerType(void) const { return fPowerType; }
