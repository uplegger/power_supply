#include "RohdeSchwarz.h"
#include "EthernetConnection.h"
#include "SerialConnection.h"

#include <iostream>
#include <string>

RohdeSchwarz::RohdeSchwarz(const pugi::xml_node configuration) : PowerSupply("RohdeSchwarz", configuration)
{
    configure();
}
RohdeSchwarz::~RohdeSchwarz()
{
    if(fConnection != nullptr)
        delete fConnection;
}

void RohdeSchwarz::configure()
{
    std::cout << "Configuring RohdeSchwarz ..." << std::endl;
    if(std::string(fConfiguration.attribute("Connection").value()).compare("Serial") == 0)
    {
        throw std::runtime_error("Shouldn't go serial");
        fConnection =
            new SerialConnection(fConfiguration.attribute("Port").value(), 9600, true, false, false, "\r\n", "\n", 5);
    }
    else if(std::string(fConfiguration.attribute("Connection").value()).compare("Ethernet") == 0)
    {
        fConnection = new SharedEthernetConnection(fConfiguration.attribute("IPAddress").value(),
                                                   std::stoi(fConfiguration.attribute("Port").value()));
    }
    else
    {
        std::stringstream error;
        error << "RohdeSchwarz: Cannot implement connection type " << fConfiguration.attribute("Connection").value()
              << ".\n"
              << "Possible values are Serial or Ethernet";
        throw std::runtime_error(error.str());
    }

    for(pugi::xml_node channel = fConfiguration.child("Channel"); channel; channel = channel.next_sibling("Channel"))
    {
        std::string inUse = channel.attribute("InUse").value();
        if(inUse.compare("Yes") != 0 && inUse.compare("yes") != 0)
            continue;
        std::string id = channel.attribute("ID").value();
        std::cout << __PRETTY_FUNCTION__ << "Configuring channel: " << id << std::endl;
        PowerSupply::fChannelMap.emplace(id, new RohdeSchwarzChannel(fConnection, channel));
    }
}

RohdeSchwarzChannel::RohdeSchwarzChannel(Connection* connection, const pugi::xml_node configuration)
    : Channel(configuration), fConnection(connection)
{
    fChannelCommand = std::string("INST:NSEL ") + fConfiguration.attribute("Channel").value() + "\n";
}
RohdeSchwarzChannel::~RohdeSchwarzChannel() {}

// void RohdeSchwarzChannel::reset() {
//     fConnection->write("*RST");
//     std::cout << "Sent reset" << std::endl;
// }

// bool RohdeSchwarzChannel::isOpen() {
//   //Workaround to set the correct channel, should be done in the constructor,
//   but is not possible std::cout << "Check power supply, select and enable
//   channel " << getOutput() << std::endl; fConnection->write("INST OUT"+
//   getOutput()); fConnection->write("OUTP:SEL 1");
// 	return fConnection->isOpen();
// }

void RohdeSchwarzChannel::turnOn()
{
    //    fConnection->write(fChannelCommand + "OUTP:GEN 1");
    fConnection->write(fChannelCommand + "OUTP ON");
    std::cout << "Turn on channel " << fConfiguration.attribute("Channel").value() << " output." << std::endl;
}

void RohdeSchwarzChannel::turnOff()
{
    //    fConnection->write(fChannelCommand + "OUTP:GEN 0");
    fConnection->write(fChannelCommand + "OUTP OFF");
    std::cout << "Turn off channel " << fConfiguration.attribute("Channel").value() << " output." << std::endl;
}

bool RohdeSchwarzChannel::isOn()
{
    std::string answer = fConnection->read(fChannelCommand + "OUTP?");
    int         result;
    sscanf(answer.c_str(), "%d", &result);
    return result;
}

void RohdeSchwarzChannel::setVoltage(float voltage)
{
    // std::cout << __PRETTY_FUNCTION__ << "-" << fChannelCommand + " VOLT " + std::to_string(voltage) << "-" <<
    // std::endl;
    fConnection->write(fChannelCommand + " VOLT " + std::to_string(voltage));
    // std::cout << "Set channel " << fConfiguration.attribute("Channel").value()
    //           << " voltage to: " << std::to_string(voltage) << std::endl;
}

void RohdeSchwarzChannel::setCurrent(float current) { setCurrentCompliance(current); }

float RohdeSchwarzChannel::getVoltage()
{
    std::string answer = fConnection->read(fChannelCommand + "VOLT?");
    float       result;
    sscanf(answer.c_str(), "%f", &result);
    return result;
}

float RohdeSchwarzChannel::getCurrent() { return getCurrentCompliance(); }

void RohdeSchwarzChannel::setVoltageCompliance(float voltage) { setVoltage(voltage); }

void RohdeSchwarzChannel::setCurrentCompliance(float current)
{
    fConnection->write(fChannelCommand + "CURR " + std::to_string(current));
    // std::cout << "Set channel " << fConfiguration.attribute("Channel").value()
    //           << " current compliance: " << std::to_string(current) << std::endl;
}

float RohdeSchwarzChannel::getVoltageCompliance() { return getVoltage(); }

float RohdeSchwarzChannel::getCurrentCompliance()
{
    std::string answer = fConnection->read(fChannelCommand + "CURR?");
    float       result;
    sscanf(answer.c_str(), "%f", &result);
    return result;
}

void RohdeSchwarzChannel::setOverVoltageProtection(float maxVoltage)
{
    fConnection->write(fChannelCommand + "VOLT:PROT:LEV " + std::to_string(maxVoltage));
    // std::cout << "Set over voltage protection to: " << std::to_string(maxVoltage) << std::endl;
}

void RohdeSchwarzChannel::setOverCurrentProtection(float maxCurrent)
{
    //    fConnection->write(fChannelCommand + "CURR:PROT " + std::to_string(maxCurrent));
    std::stringstream error;
    error << "Rohde&Schwarz setOverCurrentProtection method not implemented, aborting...";
    throw std::runtime_error(error.str());
}

float RohdeSchwarzChannel::getOverVoltageProtection()
{
    std::string answer = fConnection->read(fChannelCommand + "VOLT:PROT?");
    float       result;
    sscanf(answer.c_str(), "%f", &result);
    return result;
}

float RohdeSchwarzChannel::getOverCurrentProtection()
{
    // std::string answer = fConnection->read(fChannelCommand + "CURR:PROT?");
    // float       result;
    // sscanf(answer.c_str(), "%f", &result);
    // return result;
    std::stringstream error;
    error << "Rohde&Schwarz getOverCurrentProtection method not implemented, aborting...";
    throw std::runtime_error(error.str());
}

void RohdeSchwarzChannel::setParameter(std::string parName, float value)
{
    //    fConnection->write(fChannelCommand + parName + " " + std::to_string(value));
    fConnection->write(parName + " " + std::to_string(value));
    // std::cout << "Set " << parName << " " << std::to_string(value) << std::endl;
}

void RohdeSchwarzChannel::setParameter(std::string parName, bool value)
{
    fConnection->write(fChannelCommand + parName + " " + std::to_string(value));
    //    std::cout << "Set " << parName << " " << std::to_string(value) << std::endl;
}

void RohdeSchwarzChannel::setParameter(std::string parName, int value)
{
    //    fConnection->write(fChannelCommand + parName + " " + std::to_string(value));
    fConnection->write(parName + " " + std::to_string(value));
    // std::cout << "Set " << parName << " " << std::to_string(value) << std::endl;
}

float RohdeSchwarzChannel::getParameterFloat(std::string parName)
{
    std::string answer = fConnection->read(fChannelCommand + parName);
    // std::string answer = fConnection->read(parName);
    float result;
    sscanf(answer.c_str(), "%f", &result);
    return result;
}

int RohdeSchwarzChannel::getParameterInt(std::string parName)
{
    std::string answer = fConnection->read(fChannelCommand + parName);
    int         result;
    sscanf(answer.c_str(), "%d", &result);
    return result;
}

bool RohdeSchwarzChannel::getParameterBool(std::string parName)
{
    std::string answer = fConnection->read(fChannelCommand + "OUTP?");
    int         result;
    sscanf(answer.c_str(), "%d", &result);
    return result;
}

// Maybe check the tripping via the fuses
// void RohdeSchwarzChannel::isVoltTripped()
// {
//     return (measurevoltage() > getVoltsLimit()) ? true : false;
// }

// void RohdeSchwarzChannel::isCurrentTripped()
// {
//     return (measureAmps() > getAmpsLimit()) ? true : false;
// }

// void RohdeSchwarzChannel::setVoltageMode()
// {
//     std::cout << "RohdeSchwarz does not support the voltage mode feature, it
//     "
//                  "is automatically configured when the power supply reaches "
//                  "the voltage limit\n";
//     // pxar::LOG(logINFO) << "RohdeSchwarz-P does not support the voltage
//     mode
//     // feature, it is automatically configured when the power supply reaches
//     the
//     // voltage limit\n";
// }

// void RohdeSchwarzChannel::setCurrentMode()
// {
//     std::cout << "RohdeSchwarz does not support the current mode feature, the
//     "
//                  "power supply automatically operates in CC mode when it "
//                  "reaches the current limit\n";
//     // pxar::LOG(logINFO) << "RohdeSchwarz-P does not support the current
//     mode
//     // feature, the power supply automatically operates in CC mode when it
//     // reaches the current limit\n";
// }

// void RohdeSchwarzChannel::setVoltageRange(float voltage)
// {
//     std::cout << "RohdeSchwarz has a default voltage range of 32 V\n";
//     // pxar::LOG(logINFO) << "RohdeSchwarz-P has a default voltage range of
//     30
//     // V\n";
// }

// void RohdeSchwarzChannel::setCurrentRange(float current)
// {
//     std::cout << "RohdeSchwarz has a default current range of 10 A\n";
// }
